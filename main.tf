resource "azurerm_resource_group" "rg" {
    name = "teg-rg-eval-ipme"
    location = "West Europe"
}

resource "azurerm_public_ip" "public" {
    name                = "teg-public-ip"
    resource_group_name = azurerm_resource_group.rg.name
    location            = azurerm_resource_group.rg.location
    allocation_method   = "Dynamic"

}

resource "azurerm_virtual_network" "vnet" {
    name                = "teg-vnet"
    address_space       = ["10.0.0.0/16"]
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet" {
    name                 = "teg-subnet"
    resource_group_name  = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nic" {
    name                = "teg-nic"
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    ip_configuration {
        name                          = "internal"
        subnet_id                     = azurerm_subnet.subnet.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id = azurerm_public_ip.public.id
    }
}

variable USERNAME {
    type = string
}

variable PASSWORD {
    type = string
}



resource "azurerm_linux_virtual_machine" "vm" {
    name                = "teg-machine"
    tags = {student= "Giannesini_Baptiste"}
    resource_group_name = azurerm_resource_group.rg.name
    location            = azurerm_resource_group.rg.location
    disable_password_authentication = false
    size                = "Standard_B1ls"
    admin_username      = var.USERNAME
    admin_password = var.PASSWORD

    network_interface_ids = [
        azurerm_network_interface.nic.id,
    ]


    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "0001-com-ubuntu-server-focal"
        sku       = "20_04-lts-gen2"
        version   = "latest"
    }
}
